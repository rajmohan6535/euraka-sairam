<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'countries';

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function getTopContributors()
    {
        $countries = Country::where('status', 'active')->get();
        $list = [];
        foreach ($countries as $country) {
            $item['country'] = $country;
            $top_contributors = User::where('reputation', '>', '0')
                ->where('rid', $country->id)
                ->latest('reputation')
                ->simplePaginate(10);
            if (count($top_contributors) <= 0)
                continue;
            $item['has_more'] = $top_contributors->hasMorePages();
            $item['toppers'] = User::processFrendSuggestions($top_contributors);
            $list[] = $item;
        }

        return $list;
    }

    /**
     * To get all users for the country
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany(User::class, 'rid');
    }
}
